% PPE3 : generateur de playlist, en correlation avec le contexte Radio Libre Malraux
%
% Auteur  : Gregory DAVID and Geraldine TAYSSE
%%

\documentclass[12pt,a4paper,oneside,titlepage,final]{article}

\usepackage{style/layout}%
\usepackage{style/commands}%
\usepackage{style/glossaire}%

\newcommand{\MONTITRE}{Générateur de playlist}
\newcommand{\MONSOUSTITRE}{Cahier des charges fonctionnel}
\newcommand{\DISCIPLINE}{\gls{PPE} \No 3}

\usepackage[%
pdftex,%
pdftitle={\MONTITRE},%
pdfauthor={Grégory DAVID et Géraldine TAYSSE},%
pdfsubject={\MONSOUSTITRE},%
]{hyperref}

\lstset{%
  basicstyle={\scriptsize\ttfamily},%
}%

\author{%
  \begin{minipage}{1.0\linewidth}
      \begin{flushright}
          version en date du \gitAuthorIsoDate, \REVISIONS \hrule{}
      \end{flushright}
  \end{minipage}\\
  \begin{minipage}{0.5\linewidth}
      \noindent\href{mailto:groolot@groolot.net}{Grégory \textsc{David}}\\
      \noindent\href{mailto:geraldine.taysse@ac-nantes.fr}{Géraldine
        \textsc{Taysse}}
  \end{minipage}
  \begin{minipage}{0.5\linewidth}
      \begin{flushright}
          \Glsentrydesc{LYCMALRAUX}\\
          3 rue de Beau Soleil\\
          72700 Allonnes\\
      \end{flushright}
  \end{minipage}
}

\title{%
  \begin{flushright}
      \noindent{\Huge {\bf \MONTITRE}} \\
      \noindent{\huge \MONSOUSTITRE} \\
      \noindent{\large \DISCIPLINE} \\
  \end{flushright}
}

% \printanswers\groolotPhiligranne{CORRECTION}

\begin{document}
\begin{titlepage}
    % Page de titre
    \maketitle
\end{titlepage}

% Copyright
\include{copyright}

% Contenu
\HEADER{\MONTITRE}{\DISCIPLINE}
\rhead{\MAILGREGORY\\Géraldine \bsc{Taysse}}%

\part{Contexte}
\label{sec:contexte}
La section BTS \gls{SIO} du \gls{LYCMALRAUX} gère, maintient et anime
la Radio Libre de l'établissement.

La nature des morceaux qui sont écoutés par les auditeurs provient du
travail du \emph{programmateur radio} dont la responsabilité est de
concevoir l'enchaînement musical diffusé.

Pour cela, le \emph{programmateur radio} utilise le logiciel
\emph{VLC}. Il ajoute à la main tous les morceaux qu'il souhaite
diffuser. Une fois ajoutés dans la \emph{liste de lecture} de
\emph{VLC}, le \emph{programmateur radio} ordonne les morceaux selon
la dynamique qu'il souhaite donner, et enfin il sauvegarde cette
\emph{liste de lecture} (\emph{playlist}) dans le format \emph{M3U} et
le stocke dans le système de fichier à l'endroit défini par l'équipe
de la radio.

\begin{quote}
    \emph{Tout cela est bien fastidieux}, se dit le
    \emph{programmateur radio}.
\end{quote}

Non content de ce dur labeur, le \emph{programmateur radio} en appel
aux compétences d'un binôme de développeurs pour lui offrir un outil
de génération automatique de \emph{playlist}.

\part{Projet}
\label{sec:projet}
L'objectif global est de fournir à l'organisation un outil de
génération de \emph{playlist} musicale au format initial en
M3U\footnote{voir
  \url{http://forums.winamp.com/showthread.php?threadid=65772}} et
XSPF\footnote{voir \url{http://xspf.org/xspf-v1.html}} afin de
correspondre aux attentes techniques de \emph{VLC}.

Par ailleurs, l'avantage de ces formats de \emph{playlist} est qu'ils
sont standardisés et sont ainsi lisibles depuis la majorité des
lecteurs multimedia.

\section{Listes exhaustives des éléments et contraintes}
\label{sec:elementscontraintes}
L'environnement de fonctionnement est un système d'exploitation
\gls{GNULINUX} dont l'interface graphique est extrêmement peu
utilisée. La majorité des applications et programmes employés le sont
en interface \gls{CLI}.

La seule chose dont nous disposons est une base de données des
morceaux musicaux regroupant les champs suivants : \emph{Id},
\emph{Titre}, \emph{Album}, \emph{Artiste}, \emph{Genre},
\emph{Sous-Genre}, \emph{Durée}, \emph{Polyphonie}, \emph{Format},
\emph{Chemin}.

Cette base de données est stockée en ligne sur le SGBD PostgreSQL à
l'adresse \texttt{172.16.99.2} et écoutant sur le port \texttt{5432}.

La structure et le jeu de données de la base sont disponibles au
téléchargement sur :
\url{https://framagit.org/sio/ppe3-generateur-de-playlist/blob/master/RadioLibre.sql}.

Vous trouverez dans le \lstlistingname \vref{lst:SQL} la structure de
la base de données au format SQL/92.

L'organisation utilise exclusivement des outils Libres et souhaite que
ses développements soient Libres eux aussi.

\lstinputlisting[caption={Script SQL de la structure de la base de
  données employée}, language={SQL}, linerange={1-14},
label={lst:SQL}]{RadioLibre.sql}

\section{Expression fonctionnelle du besoin}
\label{sec:besoins}

\subsection{Fonctions de service principales}
\label{sec:fonctionsservice}
Fournir un outil permettant de générer des playlists basées sur des
critères de conception :
\begin{itemize}
    \item durée totale de la playlist,
    \item quantité d'un genre,
    \item quantité d'un sous-genre,
    \item quantité d'un artiste,
    \item quantité d'un album,
    \item quantité d'un titre,
    \item quantité d'un élément dont le motif correspond à celui
    fourni selon les règles des expressions
    rationnelles\footnote{aussi appelée \emph{regular expressions}}
    étendues compatibles PERL (par exemple : tous les albums dont le
    nom contient ``China'' ou ``china'' ou ``Chine'' ou ``chine'' :
    \texttt{.*[Cc]hin[ae].*}, ou alors tous les morceaux dont le genre
    commence par ``Roc'' ou ``roc'' : \texttt{\^{}[Rr]oc.*}~).
\end{itemize}

\subsection{Contraintes}
\label{sec:fonctionscontraintes}
\subsubsection{Utilisation}
\label{sec:licence}
Le programme doit être \emph{Libre} et adopter la licence \emph{GNU
  GPL
  v3}\footnote{\url{http://www.gnu.org/licenses/quick-guide-gplv3.html}},
au sens o\`u il doit offrir les libertés suivantes :
\begin{itemize}
    \item copie,
    \item analyse,
    \item modification,
    \item rediffusion des modifications.
\end{itemize}

Le programme \textbf{ne peut pas devenir \emph{non-libre}}.

\subsubsection{Documentations}
\label{sec:documentations}
\begin{itemize}
    \item la \emph{documentation technique} doit être rédigée en
    \emph{anglais} et intégrée au code source,
    \item la \emph{documentation utilisateur} rédigée en
    \emph{anglais} et correctement illustrée doit être disponible dans
    les formats suivants :
    \begin{itemize}
        \item l'affichage d'une documentation succinte rappelant les
        différentes options disponibles pour le programme, appelée à
        l'aide de l'option \texttt{--help} en paramètre de la ligne de
        commande (utiliser la bibliothèque logicielle adaptée en
        fonction du langage),
        \item une page de manuel rédigée en \LaTeX{} et convertie en
        \texttt{manpage} à l'aide du convertisseur
        \texttt{latex2man}\footnote{voir à ce sujet
          \url{http://mirrors.rit.edu/CTAN/support/latex2man/latex2man.html}
          ou la documentation en ligne de commande \texttt{texdoc
            latex2man}}, contenant alors la documentation détaillée de
        l'utilisation du programme,
        \item une version \texttt{HTML} de la documentation issue de
        la page de manuel\footnote{à l'aide de : \texttt{latex2man
            -H}},
        \item une version PDF de la documentation issue de la page de
        manuel\footnote{\texttt{latex2man} permet de faire une sortie
          en \LaTeXe{} qu'il suffit ensuite de compiler en PDF avec
          \texttt{pdflatex}}.
    \end{itemize}
\end{itemize}

\subsubsection{\gls{IHM}}
\label{sec:IHM}
L'interface du programme est au minimum en \gls{CLI}. Ainsi le
paramétrage du fonctionnement se fait par le passage d'options et
d'arguments sur la ligne de commande, par exemple :
\begin{lstlisting}[numbers=none]
./generateur --duration=120 --genre="[Rr]ock",30 --type=M3U --type=XSPF ma_playlist
\end{lstlisting}

Par ailleurs, l'\gls{IHM} est écrite et documentée en langue
\emph{anglaise}, tandis que nous fournirons des traductions de
l'\gls{IHM} du programme dans la langue \emph{française} au moins.

\subsubsection{Technologies/Méthodes}
\label{sec:technologiques}
Le programme doit utiliser les technologies, validées par le
commanditaire (langages, protocoles, services, méthodologies,
\dots{}), suivantes :
\begin{description}
    \item[Langage :] \texttt{C/C++} ou \texttt{Python}
    \item[Langues de l'\gls{IHM} :] \emph{anglaise} (originale),
    \emph{française} (traduction)
    \item[Méthode de conception :] Programmation Impérative modulaire
    (\texttt{C} uniquement) ou Orientée Objet (\texttt{C/C++} et
    \texttt{Python}), avec conception de bibliothèques logicielles
    lorsque cela est pertinent
    \item[Langage de requêtage :] SQL (avec les spécificités\footnote{voir à ce sujet
      \url{http://www.postgresql.org/docs/9.1/static/functions-matching.html}
      pour l'usage des \emph{regular expressions}} du
    SGBD)
    \item[SGBD/Stockage des données :] PostgreSQL
    \item[Journalisation :] des actions menées suivant leur niveau de
    criticité, effectuée dans le \texttt{syslog} du système hôte
    \item[Bibliothèques :]
    \begin{description}
        \item
        \item[C/C++ :] \texttt{argp}\footnote{Intégrée à la
          \texttt{libc}, offre la gestion des Options et Arguments sur
          la ligne de commandes :
          \url{https://www.gnu.org/software/libc/manual/html_node/Argp.html}},
        \texttt{gettext}\footnote{Intégrée à la \texttt{libc}, offre
          les modalités de traduction :
          \url{http://www.gnu.org/software/gettext/manual/gettext.html}},
        \texttt{libxml2}\footnote{Bibliothèque \texttt{C} permettant
          l'interprétation des fichiers XML :
          \url{http://xmlsoft.org/}},
        \texttt{libxerces-c}\footnote{Bibliothèque \texttt{C++}
          permettant l'interprétation et la création des fichiers XML
          : \url{http://xerces.apache.org/xerces-c/}},
        \texttt{syslog}\footnote{Journalisation système et/ou fichier,
          voir la page de manuel : \texttt{man 3 syslog} et la
          documentation sur
          \url{http://www.gnu.org/software/libc/manual/html_node/Syslog.html}},
        \texttt{libboost-log}\footnote{Journalisation local ou
          distante : \url{www.boost.org/libs/log}},
        \texttt{libpq}\footnote{API en C à PostgreSQL :
          \url{http://www.postgresql.org/docs/9.1/interactive/libpq.html}},
        \texttt{libsoci}\footnote{Bibliothèque \texttt{C++} d'accès
          aux SGBD (PostgreSQL, MySQL, DB2, Firebird, ODBC (interface
          générique), Oracle, SQLite3) :
          \url{http://soci.sourceforge.net}}

        \item[Python :] \texttt{lxml}\footnote{\url{http://lxml.de/}},
        \texttt{argparse}\footnote{\url{http://docs.python.org/3/library/argparse.html}},
        \texttt{gettext}\footnote{Module de base de \texttt{Python},
          offre les modalités de traduction :
          \url{https://docs.python.org/3.4/library/i18n.html}},
        \texttt{logging}\footnote{\url{http://docs.python.org/3/library/logging.html}
          et\newline
          \url{https://docs.python.org/3.2/library/logging.handlers.html\#logging.handlers.SysLogHandler}},
        \texttt{psycopg2}\footnote{\url{http://pythonhosted.org/psycopg2/}},
        \texttt{sqlalchemy}\footnote{\url{http://www.sqlalchemy.org/}}
    \end{description}
    \item[Documentation technique :]
    \begin{description}
        \item
        \item[C/C++ :] au format interprétable par
        Doxygen\footnote{\url{http://www.stack.nl/~dimitri/doxygen/}}
        \item[Python :] PyDoc
        \item[Langue de la documentation :] \emph{anglaise}
    \end{description}
    \item[Documentation utilisateur :] rédigée en langue
    \emph{anglaise}, elle doit respecter les directives données en
    \vref{sec:documentations}.
\end{description}

\clearpage
\section{Couverture du référentiel de certification \gls{SIO}}
\label{sec:couverturesio}

Ce \gls{PPE} permet d'aborder ou de couvrir les activités et
compétences suivantes :
\begin{enumerate}
    \item [\textbf{A1.1.1}] Analyse du cahier des charges d'un service
    à produire
    \item [\textbf{A1.2.2}] Rédaction des spécifications techniques de
    la solution retenue
    \item [\textbf{A1.2.4}] Détermination des tests nécessaires à la
    validation d'un service
    \item [\textbf{A1.3.4}] Déploiement d'un service
    \item [\textbf{A1.4.1}] Participation à un projet
    \item [\textbf{A1.4.2}] Évaluation des indicateurs de suivi d'un
    projet et justification des écarts
    \item [\textbf{C4.1.1.1}] Identifier les composants logiciels
    nécessaires à la conception de la solution
    \item [\textbf{A4.1.2}] Conception ou adaptation de l'interface
    utilisateur d'une solution application
    \item [\textbf{C4.1.3.2}] Implémenter le schéma de données dans un
    SGBD
    \item [\textbf{C4.1.3.4}] Manipuler les données liées à la
    solution applicative à travers un langage de requête
    \item [\textbf{A4.1.5}] Prototypage de composants logiciels
    \item [\textbf{A4.1.6}] Gestion d'environnements de dévelopement
    et de test
    \item [\textbf{A4.1.7}] Développement, utilisation ou adaptation
    de composants logiciels
    \item [\textbf{A4.1.9}] Rédaction d'une documentation technique
    \item [\textbf{A4.1.10}] Rédaction d'une documentation
    d'utilisation
    \item [\textbf{A5.2.1}] Exploitation des référentiels, normes et
    standards adoptés par le prestataire informatique (ici le
    commanditaire)
    \item [\textbf{A5.2.4}] Étude d'une technologie, d'un composant,
    d'un outil ou d'une méthode
\end{enumerate}

\clearpage
\printglossaries{}

% Licence
% \include{licence}

\end{document}
